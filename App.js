import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  StatusBar,
  TextInput,
  Image,
  Dimensions
} from 'react-native'

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen'
import Reader from './components/reader'
import Connector from './services/connector'
import NFC from 'react-native-nfc'
import { logo2 } from './assets/index'
const connector = new Connector()
const {
  width,
  height
} = Dimensions.get('window')

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 0,
      tagValue: '',
      inputToken: '',
      token: '',
      inputDisabled: false,
      menu: null, // register, reader
      loading: '',
      events: [],
      zones: [],
      selectedEvent: null,
      selectedZone: null,
      tag: null
    }
  }
  componentDidMount() {
    NFC.addListener((payload) => {
      let code = payload.data[0][0].data
        .split('sig')[1]
        .split('=')[1]
      this.setState({
        tag: code
      })
    })
  }
  resetTag = () => {
    this.setState({
      tag: null
    })
  }
  onCheckCode = async () => {
    try {
      this.setState({
        inputDisabled: true
      })
      let result = await connector.post('onCheckCode', {
        code: this.state.inputCode
      })
    } catch (e) {

    }
  }
  onGetActiveEvents = async () => {
    this.setState({
      events: [],
      loading: 'Loading event list'
    })
    let result = await connector.get('onGetActiveEvents')
    if (!result.error) {
      this.setState({
        events: result,
        loading: ''
      })
    }
  }
  onGetActiveZones = async (eventId) => {
    this.setState({
      loading: 'Loading zone list'
    })
    let result = await connector.post('onGetActiveZones', {
      created_time: eventId
    })
    if (!result.error) {
      this.setState({
        zones: result,
        loading: ''
      })
    }
  }
  onKeyDown = (e) => {
    if (e.nativeEvent.key === 'Enter') {
      this.onCheckToken()
    }
  }
  setCode = (val) => {
    this.setState({
      inputCode: val
    })
  }
  setMenu = (menu) => {
    if (menu === 'reader') {
      this.onGetActiveEvents()
    }
    this.setState({
      menu: menu
    })
  }
  getEventSelector = () => {
    let { events } = this.state
    const buttonStyle = {
      width: 300,
      fontSize: 18,
      marginTop: 10,
      color: 'red'
    }
    if (events.length == 0) {
      return <View>
        There are no started events
      </View>
    }
    return events.map((event) => {
      return <View key={'event_' + event.created_time} style={buttonStyle}>
        <Button
          onPress={() => {
            this.onGetActiveZones(event.created_time)
            this.setState({
              selectedEvent: event
            })
          }}
          title={event.topic}
        />
      </View>
    })
  }
  getZoneSelector = () => {
    let { zones } = this.state
    const buttonStyle = {
      width: 300,
      fontSize: 18,
      marginTop: 10,
      color: 'red'
    }
    return zones.map((zone) => {
      return <View key={'zone_' + zone.created_time} style={buttonStyle}>
        <Button
          onPress={() => {
            this.setState({
              selectedZone: zone
            })
          }}
          title={zone.name}
        />
      </View>
    })
  }
  back = () => {
    let nextMenu = this.state.menu
    let { menu, selectedEvent: event, selectedZone: zone } = this.state
    if (menu === 'register' || menu === 'reader') {
      if (menu === 'reader') {
        if (zone) {
          this.setState({
            selectedZone: null,
          })
        } else if (event) {
          this.onGetActiveEvents()
          this.setState({
            selectedEvent: null,
            events: [],
            zones: []
          })
        } else {
          nextMenu = ''
        }
      } else {
        nextMenu = ''
      }
    }
    this.setState({
      menu: nextMenu
    })
  }
  render() {
    const buttonStyle = {
      width: 300,
      fontSize: 18,
      marginTop: 10,
      color: 'red'
    }
    const mainViewStyle = {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white',
    }
    if (this.state.loading) {
      return <View style={mainViewStyle}>
        <Text>
          {this.state.loading}
        </Text>
      </View>
    }
    return (
      <View
        style={mainViewStyle}>
        <View style={{ height: height, width: width, position: "absolute" }}>
          <Image style={{ position: 'absolute', bottom: 35, left: 10, width: width * 0.50 }} source={logo2} resizeMode="contain" />
        </View>
        {
          this.state.menu || this.state.loading ?
            null :
            [
              <Text key={"text-menu"} style={{ fontSize: 24 }}>Please select menu</Text>,
              <View
                key={"view-menu"}
                style={buttonStyle}
              >
                <Button
                  disabled={true}
                  onPress={() => { this.setMenu('register') }}
                  color="#2196F3"
                  fontSize={24}
                  title="Register"
                />
              </View>,
              <View
                key={"view-select-menu"}
                style={buttonStyle}
              >
                <Button
                  onPress={() => { this.setMenu('reader') }}
                  color="#2196F3"
                  title="Reader (Enter/Quit)"
                />
              </View>
            ]
        }
        {
          this.state.menu === 'reader' && !this.state.selectedEvent ?
            (
              this.state.events.length > 0 ?
                [
                  <Text key={"text-select-event"} style={{ fontSize: 24 }}>Please select event</Text>,
                  this.getEventSelector()
                ]
                :
                <Text>
                  There are no started events
              </Text>
            )
            :
            null
        }
        {
          this.state.menu === 'reader' && this.state.selectedEvent && !this.state.selectedZone ?
            (
              this.state.zones.length > 0 ?
                [
                  <Text key={"text-select-zone"} style={{ fontSize: 36 }}>{this.state.selectedEvent.topic}</Text>,
                  <Text key={"text-select-zone"} style={{ fontSize: 24, marginBottom: 20 }}>Please select zone</Text>,
                  <View style={{maxHeight:"50%", marginBottom: 100}}>
                    <ScrollView style={{flexGrow:0}}>{this.getZoneSelector()}</ScrollView>
                  </View>
                ]
                :
                [
                  <Text key={"text-select-zone"} style={{ fontSize: 36 }}>{this.state.selectedEvent.topic}</Text>,
                  <Text>
                    There are no zones
                  </Text>
                ]
            )
            :
            null
        }
        {
          this.state.menu === 'reader' && this.state.selectedEvent && this.state.selectedZone ?
            <Reader resetTag={this.resetTag} tag={this.state.tag} key={"reader"} zone={this.state.selectedZone} event={this.state.selectedEvent} />
            :
            null
        }
        {
          this.state.loading ?
            <View>
              <Text>
                {this.state.loading}
              </Text>
            </View>
            :
            this.state.menu ?
              <View style={{ position: 'absolute', bottom: 15, right: 10, width: 100 }}>
                <Button
                  onPress={this.back}
                  color={"grey"}
                  title={'Back'}
                />
              </View>
              :
              null
        }
      </View>
    )
  }
}

export default App
