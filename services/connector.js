import config from '../config_deployment.json'
const url = config.serverAddress
class Connector {
    async getHeaders() {
        let headers = {
            headers: {
                Authorization: '',
                "Content-Type": 'application/json'
            }
        }
        if (!headers.headers.Authorization) {
            delete headers.headers.Authorization
        }
        return headers
    }

    async get(service) {
        const headers = await this.getHeaders()
        try {
            let response = await fetch(url + '/' + service, {
                method: 'GET',
                headers: headers.headers
            })
            const json = await response.json()
            return json
        } catch (e) {
            // console.log(e)
            alert('Service is errror, Please check details in log' + "\n" + e.message)
        }
    }
    async post(service, body, ignoreAlert) {
        const headers = await this.getHeaders()
        try {
            let response = await fetch(url + '/' + service, {
                method: 'POST',
                headers: headers.headers,
                body: JSON.stringify(body),
                mode: 'cors'
            })
            const json = await response.json()
            return json
        } catch (e) {
            console.log(e)
            alert('Service is errror, Please check details in log' + "\n" + e.message)
            return {
                error: 'service_error'
            }
        }
    }
}

export default Connector