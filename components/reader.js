import React from 'react'
import {
    View,
    Text,
    Button,
    Image,
    Dimensions
} from 'react-native'


import Connector from '../services/connector'
import { icon } from '../assets/index'
const connector = new Connector()

const {
    width,
    height
} = Dimensions.get('window')

class Reader extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            zone: this.props.zone,
            count: null,
            size: null,
            closed: null,
            message: null,
            realtimer: undefined,
            realtimeStamp: new Date().getTime(),
            debug: null,
            isCalling: false,
            scanCount: 0,
            realtimeCount: 0
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.tag && this.state.tag !== nextProps.tag) {
            this.scan(nextProps.tag)
            this.props.resetTag()
        }
    }
    componentDidMount() {
        if (this.state.event && this.state.zone) {
            let realtimeStamp = new Date().getTime()
            this.setState({ realtimeStamp })
            setTimeout(() => {
                this.realtime(realtimeStamp)
            }, 10);
        }
    }
    clearTimeout = () => {
        clearTimeout(this.realtimer)
    }
    componentWillUnmount() {
        clearTimeout(this.realtimer)
    }
    scan = async (code) => {
        if (this.state.isCalling) {
            return
        }
        this.setState({
            isCalling: true
        })
        this.clearTimeout()
        this.setState({
            scanCount: this.state.scanCount + 1
        })
        let result = await connector.post('scanNative', {
            event: this.state.event,
            zone: this.state.zone.created_time,
            code: {
                signature: code.slice(0, 14),
                fullCode: code
            }
        })
        const realtimeStamp = new Date().getTime()
        if (!result.error) {
            let text = ''
            if (result.text.toLowerCase().includes('full')) {
                text = 'Full'
            }
            this.setState({
                closed: false,
                size: result.size,
                count: result.count,
                message: {
                    status: result.status,
                    text: result.text.toLowerCase().includes('full') ? 'You can\'t access this zone' : result.text
                },
                realtimeStamp: realtimeStamp,
                isCalling: false
            })
            setTimeout(() => {
                this.realtime(realtimeStamp)
            }, 1700)
        } else {
            setTimeout(() => {
                this.realtime(realtimeStamp)
            }, 1700)
            this.setState({
                isCalling: false
            })
        }
    }
    realtime = (timeStamp) => {
        const { event, zone } = this.state
        this.setState({
            debug: timeStamp + ' and ' + this.state.realtimeStamp
        })
        if (this.state.realtimeStamp != timeStamp) {
            return
        }
        this.setState({
            realtimeCount: this.state.realtimeCount + 1
        })
        connector.post('realtimeNative', {
            selectedEvent: event.created_time,
            selectedZone: zone.created_time,
            fastMode: true
        }, true).then((result) => {
            if (this.state.realtimeStamp != timeStamp) {
                return
            }
            if (!result.error) {
                this.setState({
                    count: result.count,
                    size: result.size,
                    closed: false,
                    message: result.count == result.size ? { text: 'Full' } : null
                })
            } else {
                if (result.error === 'event_is_closed') {
                    this.setState({
                        closed: this.state.event.topic + ' is closed.'
                    })
                } else {
                    this.setState({
                        closed: this.state.zone.name + ' is unavailable.'
                    })
                }
            }
        })
        this.realtimer = setTimeout(() => {
            this.realtime(timeStamp)
        }, 1000)
    }

    render() {
        if (this.state.closed) {
            return <Text style={{ fontSize: 36 }}>
                {this.state.closed}
            </Text>
        }
        return [
            this.state.closed === null ?
                <Text>
                    Loading Zone Detail
                    </Text>
                :
                [
                    false && <View style={{ position: "absolute", top: 0.02 * height, right: 10 }}>
                        <Text>
                            scan:
                            {
                                this.state.scanCount
                            }
                        </Text>
                        <Text>
                            realtime:
                            {
                                this.state.realtimeCount
                            }
                        </Text>
                    </View>,
                    false && <View style={{ position: "absolute", top: 0.02 * height, left: 10 }}>
                        <Button onPress={() => { this.scan('394A210015A79C') }} title="1"></Button>
                        <Button onPress={() => { this.scan('394A210015C630') }} title="2"></Button>
                        <Button onPress={() => { this.scan('394A210015C631') }} title="3"></Button>
                        <Text>
                            {
                                this.state.debug
                            }
                        </Text>
                    </View>,
                    <View style={{ position: "absolute", top: 0.05 * height }}>
                        <Text style={{ fontSize: 0.06 * height, textAlign: "center" }}>
                            {this.state.event.topic}
                        </Text>
                    </View>,
                    <View style={{ position: "absolute", top: 0.12 * height }}>
                        <Text style={{ fontSize: 0.035 * height, textAlign: "center" }}>
                            {this.state.zone.name}
                        </Text>
                    </View>,
                    <View style={{ position: "absolute", top: 0.2 * height }}>
                        <Text style={{ fontSize: 0.025 * height, textAlign: "center" }}>
                            Participants
                        </Text>
                        <Text style={{ fontWeight: "bold", fontSize: 0.060 * height, textAlign: "center" }}>
                            {this.state.count} / {this.state.size}
                        </Text>
                    </View>,
                    (
                        this.state.message ?
                            <View style={{ position: "absolute", top: 0.6 * height }}>
                                <Text style={{ fontSize: 0.04 * height }}>
                                    {this.state.message.text}
                                </Text>
                            </View>
                            :
                            <View>
                                <Image style={{ marginLeft: width * 0.05, width: width * 0.6 }} source={icon} resizeMode="contain" />
                            </View>
                    ),
                    (
                        this.state.message ?
                            null
                            :
                            <View style={{ position: "absolute", top: 0.75 * height }}>
                                <Text style={{ fontSize: 0.035 * height, textAlign: "center" }}>
                                    Please tap to <Text style={{ fontWeight: "bold" }}>Enter</Text> or <Text style={{ fontWeight: "bold" }}>Exit</Text>
                                </Text>
                            </View>
                    ),
                    (
                        false ?
                            <View style={{ position: "absolute", bottom: 50 }}>
                                <Button
                                    onPress={() => { this.scan('394A2100160F23000000017F000017A4') }}
                                    title={"test"}
                                />
                            </View>
                            :
                            null
                    )
                ]
        ]
    }
}

export default Reader